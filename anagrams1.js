const button = document.getElementById("findButton");
button.onclick = function() {
    let typedText = document.getElementById("input").value;
    alphaOrder = alphabetize(typedText);
    let anagrams = [];
    for (let i = 0; i < words.length; i++) {
        let anagramWord = words[i];
        let alphaOrder = alphabetize(anagramWord);
        if (alphaOrder === alphabetize(typedText)) {
            anagrams.push(anagramWord);
        }
    }


    for (let sortedWord in anagrams) {
        let span = document.createElement("span");
        let textContent = document.createTextNode(anagrams[sortedWord] + " , ");
        span.appendChild(textContent)
        document.getElementById("wordDiv").appendChild(span);
    }
}

function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}